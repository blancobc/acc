# Arcade Classics Collection

Colección de juegos clásicos adaptados a dispositivos móviles.

- Snake
- Arkanoid (generación de escenarios)
- Pacman (carga de niveles en CSV generados en tiled)

Incorpora un patrón singleton para el manejo de estados del juego y control táctil.

Realizado en Unity.